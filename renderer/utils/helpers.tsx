import PublicIcon from "@material-ui/icons/Public";
import CalendarTodayIcon from "@material-ui/icons/CalendarToday";
import WarningIcon from "@material-ui/icons/Warning";
import PeopleIcon from "@material-ui/icons/People";
import FavoriteIcon from "@material-ui/icons/Favorite";
import BusinessIcon from "@material-ui/icons/Business";
import { useTranslate } from "react-polyglot";

export const concernDisplayConfig = (internalName: string) => {
  const t = useTranslate();

  const config = {
    geography: {
      name: t("geography"),
      Image: PublicIcon,
    },
    currentEvents: {
      name: t("currentEvents"),
      Image: CalendarTodayIcon,
    },
    adversaries: {
      name: t("adversaries"),
      Image: WarningIcon,
    },
    people: {
      name: t("people"),
      Image: PeopleIcon,
    },
    community: {
      name: t("community"),
      Image: FavoriteIcon,
    },
    organizationalStructure: {
      name: t("organizationalStructure"),
      Image: BusinessIcon,
    },
  };

  return (
    config[internalName] || { name: `XXX ${internalName}`, Image: undefined }
  );
};

export const areaDisplayConfig = (internalName: string) => {
  const t = useTranslate();

  const config = {
    data: { name: t("data") },
    classification: { name: t("classification") },
    access: { name: t("access") },
    storage: { name: t("storage") },
    devices: { name: t("devices") },
    configuration: { name: t("configuration") },
    software: { name: t("software") },
    hardware: { name: t("hardware") },
    communication: { name: t("communication") },
    tools: { name: t("tools") },
    email: { name: t("email") },
    onlinePresence: { name: t("onlinePresence") },
    physicalSecurity: { name: t("physicalSecurity") },
    office: { name: t("office") },
    travel: { name: t("travel") },
    education: { name: t("education") },
    trainings: { name: t("trainings") },
    externalServices: { name: t("externalServices") },
    vendorsContractors: { name: t("vendorsContractors") },
    contingencyPlanning: { name: t("contingencyPlanning") },
    backups: { name: t("backups") },
    procedures: { name: t("procedures") },
    responsibility: { name: t("responsibility") },
    staffRoles: { name: t("staffRoles") },
    governance: { name: t("governance") },
    policies: { name: t("policies") },
    security: { name: t("security") },
    privacy: { name: t("privacy") },
    dataRetention: { name: t("dataRetention") },
    noticeChoice: { name: t("noticeChoice") },
  };

  return config[internalName] || { name: `XXX ${internalName}` };
};

export const indicatorColors = ["#34cab9", "#f4e664", "#f7a34a", "#d11a1b"];

export const gradeLevels = () => {
  const t = useTranslate();

  return [
    { name: t("baseline"), explainer: t("baselineExplainer") },
    { name: t("evolving"), explainer: t("evolvingExplainer") },
    { name: t("intermediate"), explainer: t("intermediateExplainer") },
    { name: t("advanced"), explainer: t("advancedExplainer") },
    { name: t("innovative"), explainer: t("innovativeExplainer") },
  ];
};
