import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import appReducer from "../redux/appSlice";
import basicInfoReducer from "../redux/basicInfoSlice";
import concernsReducer from "../redux/concernsSlice";
import areasReducer from "../redux/areasSlice";

let store;

const makeStore = () => {
  store = configureStore({
    reducer: {
      app: appReducer,
      basicInfo: basicInfoReducer,
      concerns: concernsReducer,
      areas: areasReducer,
    },
  });
  return store;
};

export const wrapper = createWrapper(makeStore);

export type RootState = ReturnType<typeof store.getState>;
