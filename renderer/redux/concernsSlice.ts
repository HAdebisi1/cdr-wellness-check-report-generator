import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

interface Name {
  name: string;
}

interface Concern {
  active: boolean;
  score: number;
  followupScore: number;
  notes: string;
}

interface NameAndConcern {
  name: string;
  data: Concern;
}

const defaultConcernState = () => {
  return {
    active: true,
    score: 0,
    followupScore: 0,
    notes: null,
  };
};

export const concernsSlice = createSlice({
  name: "concerns",
  initialState: {
    geography: defaultConcernState(),
    currentEvents: defaultConcernState(),
    adversaries: defaultConcernState(),
    people: defaultConcernState(),
    community: defaultConcernState(),
    organizationalStructure: defaultConcernState(),
  },
  reducers: {
    update: (state, action: PayloadAction<NameAndConcern>) => {
      state[action.payload.name] = action.payload.data;
    },
    load: (state, action: PayloadAction<Concern>) => {
      Object.assign(state, action.payload);
    },
    clear: (state, action: PayloadAction<Name>) => {
      state[action.payload.name] = defaultConcernState();
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => action.payload.concerns,
  },
});

export const { update, load, clear } = concernsSlice.actions;

export default concernsSlice.reducer;
