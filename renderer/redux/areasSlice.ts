import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

const areas = {
  data: ["classification", "access", "storage"],
  devices: ["configuration", "software", "hardware"],
  communication: ["tools", "email", "onlinePresence"],
  physicalSecurity: ["office", "travel"],
  education: ["trainings"],
  externalServices: ["vendorsContractors"],
  contingencyPlanning: ["backups", "procedures"],
  responsibility: ["staffRoles", "governance"],
  policies: ["security"],
  privacy: ["dataRetention", "noticeChoice"],
};

const defaultSectionState = () => {
  return {
    active: true,
    score: 0,
    followupScore: null,
    explanation: null,
    reasonToChange: null,
    whyItMatters: null,
    recommendations: null,
    milestones: null,
  };
};

const createInitialState = () => {
  let initialState = {};

  for (const area in areas) {
    initialState[area] = {
      active: true,
      sections: {},
    };

    for (const section of areas[area]) {
      initialState[area]["sections"][section] = defaultSectionState();
    }
  }

  return initialState;
};

interface AreaInput {}

export interface Section {
  active: boolean;
  score: number;
  followupScore: number;
  explanation: string;
  reasonToChange: string;
  whyItMatters: string;
  recommendations: string;
  milestones: string;
}

interface SectionInput {
  area: string;
  section: string;
  data?: Section;
}

export const areasSlice = createSlice({
  name: "areas",
  initialState: createInitialState(),
  reducers: {
    toggleActive: (state, action: PayloadAction<string>) => {
      const area = action.payload;
      state[area].active = !state[area].active;
    },
    update: (state, action: PayloadAction<SectionInput>) => {
      const { area, section, data } = action.payload;
      state[area]["sections"][section] = data;
    },
    load: (state, action: PayloadAction<AreaInput>) => {
      Object.assign(state, action.payload);
    },
    clear: (state, action: PayloadAction<SectionInput>) => {
      const { area, section } = action.payload;
      state[area]["sections"][section] = defaultSectionState();
    },
  },
  extraReducers: {
    [HYDRATE]: (state, action) => action.payload.areas,
  },
});

export const { toggleActive, update, load, clear } = areasSlice.actions;

export default areasSlice.reducer;
