import { Box, Container } from "@material-ui/core";
import { BottomNavigationBar } from "../components/form/BottomNavigationBar";
import { Report } from "../components/report/Report";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import { PageHeader } from "../components/form/PageHeader";
import { useTranslate } from "react-polyglot";

export default function Preview() {
  const t = useTranslate();
  const { basicInfo, concerns, areas } = useSelector(
    (state: RootState) => state
  );

  return (
    <Container maxWidth="md">
      <PageHeader title={t("preview")} />
      <Box
        style={{
          marginTop: 10,
          border: "1px solid black",
          width: "100%",
          height: 1000,
          overflow: "scroll",
          padding: 20,
        }}
      >
        <Report {...{ basicInfo, concerns, areas }} />
      </Box>
      <BottomNavigationBar back="/followup" />
    </Container>
  );
}
