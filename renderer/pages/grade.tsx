import { Container } from "@material-ui/core";
import { BottomNavigationBar } from "../components/form/BottomNavigationBar";
import { PageHeader } from "../components/form/PageHeader";
import { RiskLevelSummary } from "../components/form/RiskLevelSummary";
import { GradeLevelSelector } from "../components/form/GradeLevelSelector";
import { RootState } from "../redux/store";
import { BasicInfo } from "../redux/basicInfoSlice";
import { useSelector } from "react-redux";
import { useTranslate } from "react-polyglot";

export default function Grade() {
  const t = useTranslate();
  const basicInfo: BasicInfo = useSelector(
    (state: RootState) => state.basicInfo
  );
  const areas = useSelector((state: RootState) => state.areas);

  return (
    <Container maxWidth="sm">
      <PageHeader
        title={t("assignGrade")}
        description={t("assignGradeExplainer")}
        bodyStyle={{ textAlign: "initial" }}
      >
        <RiskLevelSummary areas={areas} />
      </PageHeader>
      <GradeLevelSelector basicInfo={basicInfo} />
      <BottomNavigationBar next="/followup" back="/risk" />
    </Container>
  );
}
