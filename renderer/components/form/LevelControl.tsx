import React, { FC } from "react";
import { Field } from "formik";
import { ToggleButtonGroup } from "formik-material-ui-lab";
import { makeStyles } from "@material-ui/core/styles";
import { ToggleButton } from "@material-ui/lab";
import { Icon } from "@material-ui/core";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import { Grid } from "@material-ui/core";
import { useTranslate } from "react-polyglot";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

const useStyles = makeStyles((_) => ({
  buttonGroup: {
    display: "flex",
    alignItems: "flex-start",
  },
  button: {
    border: 0,
    padding: 0,
    margin: 0,

    color: "black",
    "&:hover": {
      backgroundColor: `white`,
    },
    "&:active": {
      backgroundColor: `white`,
    },
    "&:focus": {
      backgroundColor: `white`,
      color: "black",
    },
  },
  indicatorLine: {
    height: 2,
    marginLeft: 0,
    marginRight: 0,
    backgroundColor: "#000000",
    flexGrow: 1,
  },
}));

interface ButtonProps {
  score: number;
  value: number;
  label: string;
}

const ButtonContents: FC<ButtonProps> = ({ score, value, label }) => (
  <Grid container direction="column">
    <Grid item>
      <Icon>
        {score === value ? (
          <RadioButtonCheckedIcon />
        ) : (
          <RadioButtonUncheckedIcon />
        )}
      </Icon>
    </Grid>
    <Grid item>{label}</Grid>
  </Grid>
);

interface Props {
  score: number;
  followupScore: number;
  followup: boolean;
  submitForm: any;
}

export const LevelControl: FC<Props> = ({
  score,
  followupScore,
  followup,
  submitForm,
}) => {
  const styles = useStyles();
  const t = useTranslate();
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <Field
      component={ToggleButtonGroup}
      name={followup ? "followupScore" : "score"}
      type="checkbox"
      exclusive
      classes={{ root: styles.buttonGroup }}
    >
      <ToggleButton
        classes={{ root: styles.button }}
        value={1}
        onClick={submitForm}
      >
        <ButtonContents
          score={followup ? followupScore : score}
          value={1}
          label={t("low")}
        />
      </ToggleButton>

      <div
        style={{
          height: 3,
          marginTop: 16,
          marginLeft: isRTL ? -15 : -8,
          marginRight: isRTL ? -15 : -19,
          backgroundColor: "black",
          width: 60,
          zIndex: 1,
        }}
      />

      <ToggleButton
        classes={{ root: styles.button }}
        value={2}
        onClick={submitForm}
      >
        <ButtonContents
          score={followup ? followupScore : score}
          value={2}
          label={t("medium")}
        />
      </ToggleButton>

      <div
        style={{
          height: 3,
          marginTop: 16,
          marginLeft: isRTL ? -4 : -20,
          marginRight: isRTL ? -16 : -8,
          backgroundColor: "black",
          width: 60,
          zIndex: 1,
        }}
      />

      <ToggleButton
        classes={{ root: styles.button }}
        value={3}
        onClick={submitForm}
      >
        <ButtonContents
          score={followup ? followupScore : score}
          value={3}
          label={t("high")}
        />
      </ToggleButton>
    </Field>
  );
};
