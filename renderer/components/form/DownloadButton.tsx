import React, { FC } from "react";
import { RootState } from "../../redux/store";
import { useSelector } from "react-redux";
import { saveAs } from "file-saver";
import { useTheme } from "@material-ui/core/styles";
import {
  ButtonGroup,
  Typography,
  Grow,
  ClickAwayListener,
  Paper,
  Popper,
  MenuItem,
  MenuList,
} from "@material-ui/core";
import { default as MaterialButton } from "@material-ui/core/Button";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { useRouter } from "next/router";
import { ipcRenderer } from "electron";
import { useTranslate } from "react-polyglot";

export const DownloadButton: FC = () => {
  const t = useTranslate();
  const router = useRouter();
  const theme = useTheme();
  const { basicInfo, concerns, areas } = useSelector(
    (state: RootState) => state
  );
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleClick = () => {
    saveBoth();
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event: any) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const saveBoth = () => {
    // saveJSON();
    savePDF();
  };

  const savePDF = () => {
    router.push("/report");

    setTimeout(() => {
      ipcRenderer.on("wrote-pdf", () => {
        router.push("/preview");
      });
      ipcRenderer.send("write-pdf");
    }, 3000);
  };

  const saveJSON = () => {
    const out = { basicInfo, concerns, areas };
    const date = new Date();
    const dateString = `${date.getFullYear()}-${
      date.getMonth() + 1
    }-${date.getDate()}`;
    const blob = new Blob([JSON.stringify(out)], {
      type: "text/json;charset=utf-8",
    });
    saveAs(blob, `wellness-check-report-${dateString}.json`);
  };

  return (
    <>
      <ButtonGroup
        variant="contained"
        color="secondary"
        ref={anchorRef}
        aria-label="split button"
        style={{
          fontWeight: 500,
          color: "white",
          // @ts-ignore
          backgroundColor: theme.palette.softBlue.main,
          textTransform: "none",
          fontSize: 16,
          paddingTop: 2,
          paddingBottom: 2,
          border: 0,
        }}
      >
        <MaterialButton
          style={{
            fontWeight: 500,
            color: "white",
            // @ts-ignore
            backgroundColor: theme.palette.softBlue.main,
            textTransform: "none",
            fontSize: 16,
            paddingTop: 2,
            paddingBottom: 2,
          }}
          onClick={handleClick}
        >
          {t("downloadReport")}
        </MaterialButton>
        <MaterialButton
          color="primary"
          size="small"
          aria-controls={open ? "split-button-menu" : undefined}
          aria-expanded={open ? "true" : undefined}
          aria-haspopup="menu"
          onClick={handleToggle}
          style={{
            fontWeight: 500,
            color: "white",
            // @ts-ignore
            backgroundColor: theme.palette.softBlue.main,
            textTransform: "none",
            fontSize: 16,
            paddingTop: 2,
            paddingBottom: 2,
          }}
        >
          <ArrowDropDownIcon />
        </MaterialButton>
      </ButtonGroup>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === "bottom" ? "center top" : "center bottom",
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList id="split-button-menu">
                  <MenuItem disabled={false} key="pdf" onClick={savePDF}>
                    {t("downloadPDF")}
                  </MenuItem>
                  <MenuItem disabled={false} key="json" onClick={saveJSON}>
                    {t("downloadJSON")}
                  </MenuItem>
                  <MenuItem key="info" disabled={true}>
                    <Typography
                      variant="caption"
                      style={{
                        color: "#333",
                        borderTop: "1px solid #aaa",
                        paddingTop: 8,
                        wordWrap: "normal",
                      }}
                    >
                      {t("downloadReportExplainer")}
                    </Typography>
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
};
