import React, { FC } from "react";
import { Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { RiskLevelColorBar } from "./RiskLevelColorBar";
import { useTranslate } from "react-polyglot";

interface RiskLevelSummaryProps {
  areas: any;
}

export const RiskLevelSummary: FC<RiskLevelSummaryProps> = ({ areas }) => {
  const t = useTranslate();
  const theme = useTheme();
  let grades = [0, 0, 0, 0, 0];

  for (const areaKey of Object.keys(areas)) {
    const area = areas[areaKey];

    if (area.active) {
      for (const sectionKey of Object.keys(area.sections)) {
        const section = area.sections[sectionKey];
        grades[section.score]++;
      }
    }
  }

  const {
    livelyGreen,
    mustardYellow,
    floridaOrange,
    srirachaRed,
    faintBlue,
  }: any = theme.palette;

  return (
    <Grid
      container
      direction="column"
      justify="space-between"
      style={{
        width: 200,
        backgroundColor: faintBlue.main,
        padding: 10,
        paddingBottom: 0,
        textAlign: "initial",
        marginTop: 18,
        minHeight: 116,
      }}
    >
      <Typography variant="h5">{t("riskLevelSummary")}</Typography>
      <Grid
        container
        direction="row"
        wrap="nowrap"
        spacing={2}
        alignItems="baseline"
        justify="space-between"
        style={{ textAlign: "center" }}
      >
        <RiskLevelColorBar
          count={grades[1]}
          backgroundColor={livelyGreen.main}
        />
        <RiskLevelColorBar
          count={grades[2]}
          backgroundColor={mustardYellow.main}
        />
        <RiskLevelColorBar
          count={grades[3]}
          backgroundColor={floridaOrange.main}
        />
        <RiskLevelColorBar
          count={grades[4]}
          backgroundColor={srirachaRed.main}
        />
      </Grid>
    </Grid>
  );
};
