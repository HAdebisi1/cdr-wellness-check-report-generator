import React, { FC } from "react";
import { Grid, Icon } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

interface Props {
  score: number;
}

export const LevelIndicator: FC<Props> = ({ score }) => {
  const theme: any = useTheme();
  const { gray, black } = theme.palette;
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <Grid container alignItems="center">
      <Grid item>
        <Icon>
          <FiberManualRecordIcon
            style={{ zIndex: 2, color: score === 1 ? black.main : gray.main }}
          />
        </Icon>
      </Grid>
      <Grid item>
        <div
          style={{
            height: 3,
            marginTop: -4,
            marginLeft: isRTL ? -4 : -5,
            marginRight: isRTL ? -5 : -4,
            backgroundColor: gray.main,
            width: 12,
            zIndex: 1,
          }}
        />
      </Grid>
      <Grid item>
        <Icon>
          <FiberManualRecordIcon
            style={{ zIndex: 2, color: score === 2 ? black.main : gray.main }}
          />
        </Icon>
      </Grid>
      <Grid item>
        <div
          style={{
            height: 3,
            marginTop: -4,
            marginLeft: isRTL ? -5 : -4,
            marginRight: isRTL ? -4 : -5,
            backgroundColor: gray.main,
            width: 12,
            zIndex: 1,
          }}
        />
      </Grid>
      <Grid>
        <Icon>
          <FiberManualRecordIcon
            style={{ zIndex: 2, color: score === 3 ? black.main : gray.main }}
          />
        </Icon>
      </Grid>
    </Grid>
  );
};
