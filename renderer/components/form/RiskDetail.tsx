import React, { FC } from "react";
import { SeverityControl } from "./SeverityControl";
import { LimitedTextField } from "./LimitedTextField";
import { Button } from "./Button";
import { Typography, Grid } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { update, Section } from "../../redux/areasSlice";
import { useDispatch } from "react-redux";
import { toggleExpandedSection } from "../../redux/appSlice";
import { useTranslate } from "react-polyglot";

interface Props extends Section {
  area: string;
  section: string;
  nextArea: string;
  nextSection: string;
  followup: boolean;
}

export const RiskDetail: FC<Props> = (props) => {
  const {
    active,
    area,
    section,
    nextArea,
    nextSection,
    explanation,
    milestones,
    reasonToChange,
    recommendations,
    score,
    followupScore,
    whyItMatters,
    followup,
  } = props;

  const initialValues = {
    active,
    score,
    followupScore,
    explanation,
    milestones,
    reasonToChange,
    recommendations,
    whyItMatters,
  };

  const dispatch = useDispatch();
  const t = useTranslate();

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => {
        dispatch(update({ area, section, data: values }));
        setSubmitting(false);
      }}
    >
      {({ submitForm, isSubmitting }) => (
        <Form>
          <Grid container direction="column">
            <Grid item>
              <Typography variant="h5" style={{ textAlign: "initial" }}>
                {t("levelRisk")}
              </Typography>
            </Grid>
            <Grid item>
              <SeverityControl followup={followup} submitForm={submitForm} />
            </Grid>
            <Grid item>
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <Field
                    component={LimitedTextField}
                    name="explanation"
                    type="text"
                    label={t("explanation")}
                    onBlur={submitForm}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    component={LimitedTextField}
                    name="reasonToChange"
                    type="text"
                    label={t("reasonToChange")}
                    onBlur={submitForm}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    component={LimitedTextField}
                    name="recommendations"
                    type="text"
                    label={t("recommendations")}
                    onBlur={submitForm}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    component={LimitedTextField}
                    name="milestones"
                    type="text"
                    label={t("milestones")}
                    onBlur={submitForm}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container direction="row-reverse">
                <Grid item>
                  <Button
                    variant="secondary"
                    disabled={isSubmitting}
                    onClick={() => {
                      dispatch(toggleExpandedSection({ area, section }));
                      dispatch(
                        toggleExpandedSection({
                          area: nextArea,
                          section: nextSection,
                        })
                      );
                    }}
                  >
                    {t("next")}
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};
