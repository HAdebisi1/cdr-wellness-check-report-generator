import React, { FC } from "react";
import { Formik, Form, Field } from "formik";
import { RadioGroup } from "formik-material-ui";
import { useDispatch } from "react-redux";
import { BasicInfo, updateFollowupMonths } from "../../redux/basicInfoSlice";
import { FollowupPeriodOption } from "./FollowupPeriodOption";
import { useTranslate } from "react-polyglot";

interface FollowupPeriodSelectorProps {
  basicInfo: BasicInfo;
}

export const FollowupPeriodSelector: FC<FollowupPeriodSelectorProps> = ({
  basicInfo,
}) => {
  const dispatch = useDispatch();
  const t = useTranslate();

  return (
    <Formik
      initialValues={{ followupMonths: `${basicInfo.followupMonths}` }}
      onSubmit={(values, { setSubmitting }) => {
        dispatch(updateFollowupMonths(values.followupMonths));
        setSubmitting(false);
      }}
    >
      {({ submitForm, isSubmitting }) => (
        <Form>
          <Field
            component={RadioGroup}
            name="followupMonths"
            onClick={submitForm}
          >
            <FollowupPeriodOption
              value={3}
              label={`3 ${t("months")}`}
              submitting={isSubmitting}
            />
            <FollowupPeriodOption
              value={6}
              label={`6 ${t("months")}`}
              submitting={isSubmitting}
            />
            <FollowupPeriodOption
              value={9}
              label={`9 ${t("months")}`}
              submitting={isSubmitting}
            />
            <FollowupPeriodOption
              value={12}
              label={`12 ${t("months")}`}
              submitting={isSubmitting}
            />
            {/*
            <FollowupPeriodOption
              value={-1}
              label="Other"
              submitting={isSubmitting}
              /> */}
            <FollowupPeriodOption
              value={0}
              label={t("none")}
              submitting={isSubmitting}
            />
          </Field>
        </Form>
      )}
    </Formik>
  );
};
