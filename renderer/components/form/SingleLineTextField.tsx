import React, { FC } from "react";
import { TextField, TextFieldProps } from "formik-material-ui";
import { useTheme } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

export const SingleLineTextField: FC<TextFieldProps> = (props) => {
  const theme = useTheme();
  const { gray }: any = theme.palette;
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <TextField
      style={{
        width: "100%",
        marginTop: -4,
        marginBottom: 20,
      }}
      fullWidth
      InputProps={{
        style: {
          color: gray.dark,
          backgroundColor: gray.light,
          padding: 4,
          paddingLeft: isRTL ? 0 : 12,
          paddingRight: isRTL ? 12 : 0,
        },
      }}
      InputLabelProps={{
        dir: "rtl",
        style: {
          color: gray.dark,
          zIndex: 100,
          paddingTop: 4,
          marginLeft: isRTL ? 0 : 12,
          marginRight: isRTL ? 12 : 0,
          pointerEvents: "none",
        },
      }}
      {...props}
    />
  );
};
