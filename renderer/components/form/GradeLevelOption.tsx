import { Typography, Grid, Radio, FormControlLabel } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

export const GradeLevelOption = ({ grade, name, description, submitting }) => {
  const theme = useTheme();
  const { gray }: any = theme.palette;
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <Grid
      container
      wrap="nowrap"
      style={{
        borderBottom: `1px solid ${gray.main}`,
        paddingTop: 20,
        paddingBottom: 20,
      }}
    >
      <Grid item>
        <FormControlLabel
          value={`${grade}`}
          control={<Radio disabled={submitting} color="primary" />}
          disabled={submitting}
          label=""
          style={{
            marginTop: -8,
            marginLeft: isRTL ? 8 : -8,
            marginRight: isRTL ? -8 : 8,
          }}
        />
      </Grid>
      <Grid item>
        <Typography variant="h4" style={{ marginBottom: 6 }}>
          {name}
        </Typography>
        <Typography variant="body1">{description}</Typography>
      </Grid>
    </Grid>
  );
};
