import React, { FC } from "react";
import { SingleLineTextField } from "./SingleLineTextField";
import { Typography, Grid } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { useDispatch } from "react-redux";
import { BasicInfo, update } from "../../redux/basicInfoSlice";
import { useTranslate } from "react-polyglot";

interface ContactInfoProps {
  basicInfo: BasicInfo;
}

export const ContactInfo: FC<ContactInfoProps> = ({ basicInfo }) => {
  const dispatch = useDispatch();
  const t = useTranslate();

  return (
    <Formik
      initialValues={basicInfo}
      onSubmit={(values, { setSubmitting }) => {
        dispatch(update(values));
        setSubmitting(false);
      }}
    >
      {({ submitForm }) => (
        <Form>
          <Grid container direction="column">
            <Grid item>
              <Typography variant="h4" style={{ marginTop: 40 }}>
                {t("organizationContact")}
              </Typography>
            </Grid>
            <Grid item>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <Field
                    component={SingleLineTextField}
                    name="organizationContactName"
                    label={t("name")}
                    type="text"
                    onBlur={submitForm}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    component={SingleLineTextField}
                    name="organizationContactInfo"
                    label={t("phoneEmail")}
                    type="text"
                    onBlur={submitForm}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Typography variant="h4" style={{ marginTop: 20 }}>
                {t("supportContact")}
              </Typography>
            </Grid>
            <Grid item>
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <Field
                    component={SingleLineTextField}
                    name="supportContactName"
                    label={t("name")}
                    type="text"
                    onBlur={submitForm}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    component={SingleLineTextField}
                    name="supportContactInfo"
                    label={t("phoneEmail")}
                    type="text"
                    onBlur={submitForm}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};
