import React, { FC } from "react";
import { withStyles } from "@material-ui/core/styles";
import { default as MaterialButton } from "@material-ui/core/Button";

const PrimaryButton = withStyles((theme) => ({
  root: {
    fontWeight: 500,
    color: "white",
    // @ts-ignore
    backgroundColor: theme.palette.softBlue.main,
    textTransform: "none",
    fontSize: 16,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 20,
  },
}))(MaterialButton);

const SecondaryButton = withStyles((theme) => ({
  root: {
    fontWeight: "bold",
    // @ts-ignore
    color: theme.palette.softBlue.main,
    backgroundColor: "white",
    textTransform: "none",
    fontSize: 16,
  },
}))(MaterialButton);

const SmallButton = withStyles((_) => ({
  root: {
    fontWeight: "bold",
    color: "white",
    backgroundColor: "#2EBE60",
    textTransform: "none",
    fontSize: 13,
  },
}))(MaterialButton);

export const Button: FC<any> = ({ variant, children, ...props }) => {
  switch (variant) {
    case "primary":
      return <PrimaryButton {...props}>{children}</PrimaryButton>;
    case "secondary":
      return <SecondaryButton {...props}>{children}</SecondaryButton>;
    case "small":
      return <SmallButton {...props}>{children}</SmallButton>;
    default:
      return null;
  }
};
