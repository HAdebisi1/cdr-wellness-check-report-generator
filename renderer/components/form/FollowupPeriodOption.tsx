import { FC } from "react";
import { FormControlLabel, Radio } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { RootState } from "../../redux/store";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  label: {
    fontWeight: 900,
    fontSize: 16,
    color: "#666",
    borderBottom: `1px solid #ccc`,
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: 8,
    width: "100%",
  },
}));

interface FollowupPeriodOptionProps {
  value: number;
  label: string;
  submitting: boolean;
}

export const FollowupPeriodOption: FC<FollowupPeriodOptionProps> = ({
  value,
  label,
  submitting,
}) => {
  const styles = useStyles();
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <FormControlLabel
      value={`${value}`}
      control={<Radio color="primary" disabled={submitting} />}
      disabled={submitting}
      label={label}
      classes={{ label: styles.label }}
      style={{
        marginLeft: isRTL ? 0 : -10,
        marginRight: isRTL ? -8 : 0,
      }}
    />
  );
};
