import React, { FC } from "react";
import { Box, Grid, Checkbox, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { toggleActive } from "../../redux/areasSlice";
import { areaDisplayConfig } from "../../utils/helpers";
import { RootState } from "../../redux/store";
import { useDispatch, useSelector } from "react-redux";

interface Props {
  area: string;
  active: boolean;
  sections: any;
}

export const RiskListItem: FC<Props> = ({ area, active, sections }) => {
  const dispatch = useDispatch();
  const theme: any = useTheme();
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";
  const { softBlue, gray } = theme.palette;
  const joinedSections = Object.keys(sections)
    .map((section) => areaDisplayConfig(section).name)
    .join(", ");
  let formattedSections = joinedSections;

  if (Object.keys(sections).length > 1 && !joinedSections.includes("&")) {
    const lastComma = joinedSections.lastIndexOf(",");
    formattedSections =
      joinedSections.substr(0, lastComma) +
      " &" +
      joinedSections.substr(lastComma + 1);
  }

  if (!gray) {
    return null;
  }

  return (
    <Box
      style={{
        minWidth: 450,
      }}
    >
      <Grid container direction="row">
        <Grid item>
          <Checkbox
            checked={active}
            style={{ color: softBlue.main, marginTop: 4 }}
            onClick={() => dispatch(toggleActive(area))}
          />
        </Grid>
        <Grid
          item
          style={{
            borderBottom: "1px solid #ccc",
            paddingTop: 10,
            paddingBottom: 10,
            flexGrow: 1,
            marginRight: isRTL ? 0 : 10,
            marginLeft: isRTL ? 10 : 0,
          }}
        >
          <Grid container direction="column" style={{ width: "100%" }}>
            <Grid item>
              <Typography variant="h3" style={{ color: softBlue.main }}>
                {areaDisplayConfig(area).name}
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body1" style={{ color: gray.dark }}>
                {formattedSections}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};
