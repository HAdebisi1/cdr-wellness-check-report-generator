import React, { FC } from "react";
import { Grid, Box } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";

interface Props {
  score: number;
}

export const SeverityIndicator: FC<Props> = ({ score }) => {
  const theme: any = useTheme();
  const {
    livelyGreen,
    mustardYellow,
    floridaOrange,
    srirachaRed,
    gray,
  } = theme.palette;

  return (
    <Grid container alignItems="flex-end" spacing={1}>
      <Grid item>
        <Box
          style={{
            backgroundColor: score === 1 ? livelyGreen.main : gray.main,
            width: 10,
            height: 10,
          }}
        />
      </Grid>
      <Grid item>
        <Box
          style={{
            backgroundColor: score === 2 ? mustardYellow.main : gray.main,
            width: 10,
            height: 12,
          }}
        />
      </Grid>
      <Grid item>
        <Box
          style={{
            backgroundColor: score === 3 ? floridaOrange.main : gray.main,
            width: 10,
            height: 14,
          }}
        />
      </Grid>
      <Grid item>
        <Box
          style={{
            backgroundColor: score === 4 ? srirachaRed.main : gray.main,
            width: 10,
            height: 16,
          }}
        />
      </Grid>
    </Grid>
  );
};
