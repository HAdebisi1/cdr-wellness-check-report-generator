import React, { FC } from "react";
import { Typography, Grid } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-material-ui";
import { toggleExpandedConcern } from "../../redux/appSlice";
import { update } from "../../redux/concernsSlice";
import { useDispatch } from "react-redux";
import { Button } from "./Button";
import { LevelControl } from "./LevelControl";
import { makeStyles } from "@material-ui/styles";
import { useTranslate } from "react-polyglot";

interface ConcernDetailProps {
  concern: string;
  nextConcern?: string;
  score: number;
  followupScore: number;
  notes: string;
  active: boolean;
  followup: boolean;
}

const useStyles = makeStyles((_) => ({
  textField: {
    backgroundColor: "#efefef",
    marginTop: 20,
    marginBottom: 10,
    "&::before": {
      display: "none",
    },
  },
}));

export const ConcernDetail: FC<ConcernDetailProps> = ({
  concern,
  nextConcern,
  score,
  followupScore,
  active,
  notes,
  followup,
}) => {
  const initialValues = { score, followupScore, notes, active };
  const styles = useStyles();
  const dispatch = useDispatch();
  const t = useTranslate();

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => {
        dispatch(update({ name: concern, data: values }));
        setSubmitting(false);
      }}
    >
      {({ submitForm }) => (
        <Form>
          <Grid
            container
            direction="column"
            spacing={1}
            style={{ width: "100%" }}
          >
            <Grid item>
              <Typography variant="h5" style={{ textAlign: "initial" }}>
                {t("levelConcern")}
              </Typography>
            </Grid>
            <Grid item>
              <LevelControl
                score={score}
                followupScore={followupScore}
                followup={followup}
                submitForm={submitForm}
              />
            </Grid>
            <Grid item>
              <Field
                component={TextField}
                name="notes"
                type="text"
                label={t("notes")}
                onBlur={submitForm}
                fullWidth
                multiline={true}
                rows={5}
                classes={{ root: styles.textField }}
                inputProps={{ classes: { root: styles.textField } }}
              />
            </Grid>
            {nextConcern && (
              <Grid item>
                <Grid container direction="row-reverse">
                  <Grid item>
                    <Button
                      variant="secondary"
                      onClick={() => {
                        dispatch(toggleExpandedConcern(concern));
                        dispatch(toggleExpandedConcern(nextConcern));
                      }}
                    >
                      {t("next")}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            )}
          </Grid>
        </Form>
      )}
    </Formik>
  );
};
