import React, { FC } from "react";
import {
  Typography,
  Grid,
  Paper,
  Radio,
  FormControlLabel,
} from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { RadioGroup } from "formik-material-ui";
import { useSelector, useDispatch } from "react-redux";
import { load as loadBasicInfo } from "../../redux/basicInfoSlice";
import { load as loadConcerns } from "../../redux/concernsSlice";
import { load as loadAreas } from "../../redux/areasSlice";
import { RootState } from "../../redux/store";
import { updateFollowup } from "../../redux/basicInfoSlice";
import BusinessIcon from "@material-ui/icons/Business";
import Dropzone from "react-dropzone";
import { useTheme } from "@material-ui/core/styles";
import { useTranslate } from "react-polyglot";

export const ModeSelector: FC = () => {
  const t = useTranslate();
  const followup = useSelector((state: RootState) => state.basicInfo.followup);
  const dispatch = useDispatch();
  const theme = useTheme();
  const { gray }: any = theme.palette;
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  const loadConfig = (conf) => {
    // const ajv = new Ajv();
    // const validate = ajv.compile(schema);
    // const valid = validate(conf);
    const valid = true;

    if (valid) {
      const parsed = JSON.parse(conf);
      dispatch(loadBasicInfo(parsed.basicInfo));
      dispatch(loadConcerns(parsed.concerns));
      dispatch(loadAreas(parsed.areas));
    }
  };

  const fileReceived = (files) => {
    if (files.length > 0) {
      const file = files[0];
      const reader = new window.FileReader();
      reader.onload = function (event) {
        const json = event.target.result;
        loadConfig(json);
      };
      reader.readAsText(file);
    }
  };

  return (
    <Formik
      initialValues={{ followup: `${followup}` }}
      onSubmit={(values, { setSubmitting }) => {
        dispatch(updateFollowup(values.followup === "true"));
        setSubmitting(false);
      }}
    >
      {({ submitForm }) => (
        <Form>
          <Grid container direction="column" spacing={2}>
            <Grid item>
              <Typography
                variant="h4"
                style={{ marginTop: 30, marginBottom: 6 }}
              >
                {t("newOrFollowup")}
              </Typography>
              <Field component={RadioGroup} name="followup">
                <Grid container>
                  <Grid item>
                    <FormControlLabel
                      value="false"
                      control={<Radio color="primary" />}
                      label={t("newReport")}
                      onClick={submitForm}
                      style={{
                        marginLeft: isRTL ? 0 : -12,
                        marginRight: isRTL ? -12 : 0,
                      }}
                    />
                  </Grid>
                  <Grid item>
                    <FormControlLabel
                      value="true"
                      control={<Radio color="primary" />}
                      label={t("followupReport")}
                      onClick={submitForm}
                      style={{
                        marginLeft: isRTL ? -12 : 0,
                        marginRight: isRTL ? 0 : -12,
                      }}
                    />
                  </Grid>
                </Grid>
              </Field>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};
