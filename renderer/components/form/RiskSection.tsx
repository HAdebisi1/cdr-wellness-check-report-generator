import React, { FC } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import { Typography, Grid } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";
import { RiskDetail } from "./RiskDetail";
import { SeverityIndicator } from "./SeverityIndicator";
import { MultiSeverityIndicator } from "./MultiSeverityIndicator";
import { areaDisplayConfig } from "../../utils/helpers";
import { useDispatch } from "react-redux";
import { toggleExpandedSection } from "../../redux/appSlice";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((_) => ({
  accordion: {
    border: 0,
    borderRadius: 0,
    paddingTop: 8,
    marginBottom: 4,
    boxShadow: "none",
    "&::before": {
      display: "none",
    },
    borderTop: "1px solid lightgray",
  },
  accordionExpanded: {
    boxShadow: "none",
    borderRadius: 0,
    borderBottom: 0,
    borderTop: "1px solid lightgray",
  },
}));

interface Props {
  area: string;
  nextArea: string;
  active: boolean;
  sections: any;
  nextSections: any;
  expandedSections: string[];
  followup: boolean;
}

export const RiskSection: FC<Props> = ({
  area,
  nextArea,
  active,
  sections,
  nextSections,
  expandedSections,
  followup,
}) => {
  const styles = useStyles();
  const theme: any = useTheme();
  const { softBlue, black } = theme.palette;
  const dispatch = useDispatch();

  if (!softBlue) return null;

  return (
    active && (
      <Paper style={{ padding: 20, marginBottom: 20 }}>
        <Typography
          variant="h3"
          style={{
            color: softBlue.main,
            textAlign: "initial",
            marginBottom: 10,
          }}
        >
          {areaDisplayConfig(area).name}
        </Typography>
        {Object.keys(sections).map((section, i) => (
          <Accordion
            classes={{
              root: styles.accordion,
              expanded: styles.accordionExpanded,
            }}
            elevation={0}
            key={section}
            expanded={expandedSections.includes(`${area}-${section}`)}
            onChange={() => dispatch(toggleExpandedSection({ area, section }))}
          >
            <AccordionSummary id={section} style={{ padding: 0, margin: 0 }}>
              <Grid container justify="space-between">
                <Grid item>
                  <Typography
                    variant="h4"
                    style={{
                      color: black.main,
                      marginBottom: 0,
                    }}
                  >
                    {areaDisplayConfig(section).name}
                  </Typography>
                </Grid>
                <Grid item>
                  {followup ? (
                    <MultiSeverityIndicator
                      score={sections[section].score}
                      followupScore={sections[section].followupScore}
                    />
                  ) : (
                    <SeverityIndicator score={sections[section].score} />
                  )}
                </Grid>
              </Grid>
            </AccordionSummary>

            <AccordionDetails style={{ padding: 0 }}>
              <RiskDetail
                area={area}
                section={section}
                nextArea={
                  i === Object.keys(sections).length - 1 ? nextArea : area
                }
                nextSection={
                  nextSections && i === Object.keys(sections).length - 1
                    ? Object.keys(nextSections)[0]
                    : Object.keys(sections)[i + 1]
                }
                {...sections[section]}
                expandedSections={expandedSections}
                followup={followup}
              />
            </AccordionDetails>
          </Accordion>
        ))}
      </Paper>
    )
  );
};
