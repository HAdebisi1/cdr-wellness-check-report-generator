import React, { FC } from "react";
import {
  Box,
  Container,
  Grid,
  Typography,
  Dialog,
  DialogContent,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import { Button } from "./Button";
import { useTranslate } from "react-polyglot";
import {
  updateShowWelcomeDialog,
  updateShowRequirementsDialog,
} from "../../redux/appSlice";
import { LocaleSelector } from "./LocaleSelector";
import { RootState } from "../../redux/store";
import { useSelector } from "react-redux";

interface WelcomeDialogProps {
  open: boolean;
}

export const WelcomeDialog: FC<WelcomeDialogProps> = ({ open }) => {
  const { textDirection } = useSelector((state: RootState) => state.app);
  const dispatch = useDispatch();
  const t = useTranslate();

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" dir={textDirection}>
      <DialogContent style={{ padding: "40px 40px 0px 40px" }}>
        <Container maxWidth="sm">
          <Box style={{ textAlign: "center" }}>
            <img
              style={{ width: 366, height: 279, marginBottom: 20 }}
              src="/images/intro.png"
            />
            <Typography variant="h1" style={{ marginBottom: 15 }}>
              {t("welcome")}
            </Typography>
            <Typography variant="body1">{t("welcomeExplainer")}</Typography>
          </Box>
          <Grid
            container
            style={{ marginTop: 30, marginBottom: 30 }}
            spacing={2}
            direction="row-reverse"
          >
            <Grid item>
              <Button
                variant="primary"
                onClick={() => {
                  dispatch(updateShowWelcomeDialog(false));
                  dispatch(updateShowRequirementsDialog(true));
                }}
              >
                {t("continue")}
              </Button>
            </Grid>
          </Grid>
          <Grid
            container
            style={{ marginTop: 30, marginBottom: 30 }}
            spacing={2}
            direction="row"
            justify="space-around"
          >
            <Grid item>
              <LocaleSelector />
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
    </Dialog>
  );
};
