import React, { FC } from "react";
import { SummaryHeader } from "./SummaryHeader";
import { SummarySection } from "./SummarySection";
import styles from "./Summary.module.css";
import { areaDisplayConfig } from "../../utils/helpers";

interface Props {
  area: string;
  date: string;
  active: boolean;
  followup: boolean;
  sections: any;
}

export const Summary: FC<Props> = ({
  area,
  date,
  active,
  followup,
  sections,
}) => {
  if (!active) return null;

  return (
    <section className={styles.summary}>
      <SummaryHeader title={areaDisplayConfig(area).name} date={date} />
      {Object.keys(sections).map((section) => (
        <SummarySection
          key={section}
          section={section}
          followup={followup}
          {...sections[section]}
        />
      ))}
    </section>
  );
};
