import React, { FC } from "react";
import styles from "./Concern.module.css";
import { ConcernRating } from "./ConcernRating";
import { useTranslate } from "react-polyglot";

interface Props {
  concerns: any;
}

export const Concern: FC<Props> = ({ concerns }) => {
  const t = useTranslate();

  return (
    <div className={styles.concernContainer}>
      <div>
        <h4 className={styles.contextTitle}>{t("context")}</h4>
        <p className={styles.explanation}>{t("contextExplainer")}</p>
      </div>
      <div className={styles.ratingsContainer}>
        {Object.keys(concerns).map((concern) => (
          <ConcernRating
            key={concern}
            concern={concern}
            {...concerns[concern]}
          />
        ))}
      </div>
    </div>
  );
};
