import React, { FC } from "react";
import { Icon } from "@material-ui/core";
import styles from "./ConcernRating.module.css";
import { LevelIndicator } from "./LevelIndicator";
import { concernDisplayConfig } from "../../utils/helpers";

interface Props {
  concern: string;
  score: number;
  active: boolean;
}

export const ConcernRating: FC<Props> = ({ concern, score, active }) => {
  const { Image, name } = concernDisplayConfig(concern);

  if (!active) return null;

  return (
    <div className={styles.ratingContainer}>
      <Image className={styles.concernIcon} />
      <h5 className={styles.concernTitle}>{name}</h5>
      <LevelIndicator score={score} />
    </div>
  );
};
