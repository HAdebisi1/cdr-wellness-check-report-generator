import React, { FC } from "react";
import styles from "./RiskRating.module.css";
import { areaDisplayConfig } from "../../utils/helpers";
import { ColorRating } from "./ColorRating";

interface Props {
  followup: boolean;
  area: string;
  sections: any;
  active: boolean;
}

export const RiskRating: FC<Props> = ({ followup, area, sections, active }) => {
  if (!active) return null;

  return (
    <div className={styles.riskRatingContainer}>
      <div className={styles.riskRatingInnerContainer}>
        <h5 className={styles.areaTitle}>{areaDisplayConfig(area).name}</h5>
        {Object.keys(sections).map((section) => (
          <ColorRating
            key={section}
            followup={followup}
            section={section}
            {...sections[section]}
          />
        ))}
      </div>
    </div>
  );
};
