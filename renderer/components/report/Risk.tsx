import React, { FC } from "react";
import styles from "./Risk.module.css";
import { RiskRating } from "./RiskRating";
import { useTranslate } from "react-polyglot";

const colors = ["#34cab9", "#f4e664", "#f7a34a", "#d11a1b"];

interface Props {
  followup: boolean;
  areas: any;
}

export const Risk: FC<Props> = ({ followup, areas }) => {
  const t = useTranslate();

  return (
    <div className={styles.riskContainer}>
      <div className={styles.riskHeaderContainer}>
        <div>
          <h4 className={styles.riskTitle}>{t("riskAssessment")}</h4>
          <p className={styles.explanation}>
            {t("riskExplainer")}
            <em>
              <strong>{t("context")}</strong>
            </em>
            .
          </p>
        </div>
        <div className={styles.keyContainer}>
          <div className={styles.keyItem}>
            <div
              className={styles.keyColorBox}
              style={{
                backgroundColor: colors[0],
                height: 6,
              }}
            ></div>
            <p className={styles.keyText}>{t("low")}</p>
          </div>
          <div className={styles.keyItem}>
            <div
              className={styles.keyColorBox}
              style={{
                backgroundColor: colors[1],
                height: 8,
              }}
            ></div>
            <p className={styles.keyText}>{t("moderateLow")}</p>
          </div>
          <div className={styles.keyItem}>
            <div
              className={styles.keyColorBox}
              style={{
                backgroundColor: colors[2],
                height: 10,
              }}
            ></div>
            <p className={styles.keyText}>{t("moderate")}</p>
          </div>
          <div className={styles.keyItem}>
            <div
              className={styles.keyColorBox}
              style={{
                backgroundColor: colors[3],
                height: 12,
              }}
            ></div>
            <p className={styles.keyText}>{t("high")}</p>
          </div>
        </div>
      </div>
      <div className={styles.risksContainer}>
        {Object.keys(areas).map((area) => (
          <RiskRating
            key={area}
            followup={followup}
            area={area}
            {...areas[area]}
          />
        ))}
      </div>
    </div>
  );
};
