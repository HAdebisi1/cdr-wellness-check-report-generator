import React, { FC } from "react";
import { Grid, Icon } from "@material-ui/core";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import styles from "./LevelIndicator.module.css";
import { useTranslate } from "react-polyglot";

interface Props {
  score: number;
}

export const LevelIndicator: FC<Props> = ({ score }) => {
  const t = useTranslate();
  const black = "#000000";

  return (
    <Grid
      container
      direction="row"
      alignItems="center"
      style={{ marginTop: 10 }}
    >
      <Grid item>
        <Grid container direction="column" alignItems="center">
          <Icon>
            {score === 1 ? (
              <RadioButtonCheckedIcon style={{ zIndex: 2, color: black }} />
            ) : (
              <RadioButtonUncheckedIcon />
            )}
          </Icon>
          <p className={styles.levelText}>{t("low")}</p>
        </Grid>
      </Grid>
      <Grid item>
        <div className={styles.connectorLineLeft} />
      </Grid>
      <Grid item>
        <Grid container direction="column" alignItems="center">
          <Icon>
            {score === 2 ? (
              <RadioButtonCheckedIcon style={{ zIndex: 2, color: black }} />
            ) : (
              <RadioButtonUncheckedIcon />
            )}
          </Icon>
          <p className={styles.levelText}>{t("medium")}</p>
        </Grid>
      </Grid>
      <Grid item>
        <div className={styles.connectorLineRight} />
      </Grid>
      <Grid item>
        <Grid container direction="column" alignItems="center">
          <Icon>
            {score === 3 ? (
              <RadioButtonCheckedIcon style={{ zIndex: 2, color: black }} />
            ) : (
              <RadioButtonUncheckedIcon />
            )}
          </Icon>
          <p className={styles.levelText}>{t("high")}</p>
        </Grid>
      </Grid>
    </Grid>
  );
};
