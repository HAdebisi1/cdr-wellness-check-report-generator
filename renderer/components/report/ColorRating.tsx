import React, { FC } from "react";
import styles from "./ColorRating.module.css";
import { areaDisplayConfig, indicatorColors } from "../../utils/helpers";

interface Props {
  followup: boolean;
  section: string;
  score: number;
  followupScore: number;
}

export const ColorRating: FC<Props> = ({
  followup,
  section,
  score,
  followupScore,
}) => {
  const colors = indicatorColors;
  const currentScore = followup ? followupScore : score;
  return (
    <div className={styles.colorRatingContainer}>
      <p className={styles.subareaTitle}>{areaDisplayConfig(section).name}</p>
      <div className={styles.colorsContainer}>
        {colors.map((color, index) => (
          <div
            key={color}
            className={styles.colorContainer}
            style={{
              backgroundColor: color,
              opacity: currentScore === index + 1 ? 1.0 : 0.2,
              height: index * 2.5 + 12,
            }}
          />
        ))}
      </div>
    </div>
  );
};
