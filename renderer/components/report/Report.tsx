import React, { FC, useRef } from "react";
import styles from "./Report.module.css";
import { Intro } from "./Intro";
import { Concern } from "./Concern";
import { Risk } from "./Risk";
import { Context } from "./Context";
import { Summary } from "./Summary";

interface Props {
  basicInfo: any;
  concerns: any;
  areas: any;
}

export const Report: FC<Props> = ({ basicInfo, concerns, areas }) => {
  const reportRef = useRef();
  const date = basicInfo.followup
    ? basicInfo.followupAssessmentDate
    : basicInfo.originalAssessmentDate;

  return (
    <div id="report" className={styles.report} ref={reportRef}>
      <div className="reportInner">
        <Intro basicInfo={basicInfo} />
        <Concern concerns={concerns} />
        <Risk followup={basicInfo.followup} areas={areas} />
        <Context concerns={concerns} date={date} />
        {Object.keys(areas).map((area) => (
          <Summary
            key={area}
            date={date}
            area={area}
            followup={basicInfo.followup}
            {...areas[area]}
          />
        ))}
      </div>
    </div>
  );
};
