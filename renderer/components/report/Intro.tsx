import React, { FC } from "react";
import reportStyles from "./Report.module.css";
import styles from "./Intro.module.css";
import { BasicInfo } from "../../redux/basicInfoSlice";
import { format, parse } from "date-fns";
import { gradeLevels } from "../../utils/helpers";
import { useTranslate } from "react-polyglot";

interface Props {
  basicInfo: BasicInfo;
}

export const Intro: FC<Props> = ({ basicInfo }) => {
  const gradeLevel = (basicInfo.gradeLevel ?? 0) as number;
  const t = useTranslate();

  return (
    <div className={styles.introContainer}>
      <div className={styles.headerContainer}>
        <div className={styles.titleContainer}>
          <h5 className={reportStyles.heading5}>Digital Wellness Check</h5>
          <h1 className={reportStyles.heading1}>
            {basicInfo.organization ?? "Org Name"}: <br />{" "}
            {format(
              parse(
                basicInfo.followup
                  ? basicInfo.followupAssessmentDate ??
                      format(new Date(), "yyyy-MM-dd")
                  : basicInfo.originalAssessmentDate ??
                      format(new Date(), "yyyy-MM-dd"),
                "yyyy-MM-dd",
                new Date()
              ),
              "MMM dd, yyyy"
            )}
          </h1>
        </div>
        <div className={styles.scoreContainer}>
          <h6 className={styles.scoreLabel}>{t("grade")}</h6>
          <h4 className={styles.score}>
            {gradeLevel === 0 ? "" : gradeLevels()[gradeLevel - 1].name}
          </h4>
        </div>
      </div>
      <div className={styles.supportFollowupContainer}>
        <div className={styles.supportContainer}>
          <span className={styles.fieldName}>{t("supportContact")}</span>
          <span className={styles.fieldValue}>
            {basicInfo.supportContactName}
          </span>
          <span className={styles.fieldValue}>
            {basicInfo.supportContactInfo}
          </span>
        </div>
        {basicInfo.followupMonths && basicInfo.followupMonths > 0 ? (
          <div className={styles.followupContainer}>
            <span className={styles.fieldName}>{t("followup")}</span>
            <span className={styles.fieldValue}>
              {basicInfo.followupMonths} {t("months")}
            </span>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};
