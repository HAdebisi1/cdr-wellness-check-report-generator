# Wellness Check Report Generator

## SYNOPSIS

The Wellness Check Report Generator is a tool to help the community create beautiful risk assessments. For the past year and a half, The Center for Digital Resilience has been progressively deploying and developing the Digital Wellness Check, a methodology and framework arising from a desire to improve the experience of assessing an organization’s digital security posture. The report output of these Digital Wellness Checks - or really any digital risk assessment - can be created using this tool.

## RUNNING THE TOOL

The Wellness Check Report Generator is designed to be downloaded locally to a device and run from there. The tool is compatible with Windows, MacOS, and Linux Operating Systems. Please visit the Releases page to get the latest version: https://gitlab.com/digiresilience/digital-wellness/wellness-check-report-generator/-/releases.

## LICENSING

The Wellness Check Report Generator is a free software project licensed under the GNU Affero General Public License v3.0 (GNU AGPLv3) by The Center for Digital Resilience and Guardian Project.

## FEEDBACK AND BUGS

We’re so glad you’re one of the first users of the tool! We expect the community to have lots of useful feedback and even to find some issues. Please report any feedback and bugs via the following methods:
- GitLab Issues
- Email: info [at] digiresilience.org

