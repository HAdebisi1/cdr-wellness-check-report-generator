import { app, BrowserWindow, ipcMain, dialog } from "electron";
import serve from "electron-serve";
import fs from "fs";
import { createWindow } from "./helpers";
// import { AppUpdater } from "./updater";

const isProd: boolean = process.env.NODE_ENV === "production";

app.setName("Digital Wellness Check");

if (isProd) {
  serve({ directory: "app" });
} else {
  app.setPath("userData", `${app.getPath("userData")} (development)`);
}

(async () => {
  await app.whenReady();

  const mainWindow = createWindow("main", {
    width: 1000,
    height: 600,
  });

  if (isProd) {
    await mainWindow.loadURL("app://./intro.html");
  } else {
    const port = process.argv[2];
    await mainWindow.loadURL(`http://localhost:${port}/intro`);
  }
})();

app.on("window-all-closed", () => {
  app.quit();
});

ipcMain.on('write-pdf', function (event) {
  const win = BrowserWindow.fromWebContents(event.sender);
  const options = {
    marginsType: 0,
    pageSize: "A4",
    printBackground: true,
    printSelectionOnly: false,
    landscape: false,
  };
  const date = new Date();
  const dateString = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  win.webContents
    .printToPDF(options)
    .then(async (data) => {
      const fileName = `wellness-check-report-${dateString}.pdf`
      const { filePath } = await dialog.showSaveDialog(win, { defaultPath: fileName });
      fs.writeFileSync(filePath, data, { encoding: "utf-8" });
      console.log("PDF Generated Successfully");
      event.sender.send('wrote-pdf', fileName);
    })
    .catch((error) => {
      console.log(error);
      event.sender.send('wrote-pdf', null);
    });
})

